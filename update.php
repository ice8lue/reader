<?php
	
	function do_extract($source,$dest) {
		$files = scandir($source);
		$src = $source.'/';
		$destination = $dest.'/';
		foreach ($files as $file) {
		  if (!in_array($file, array(".",".."))){ 
			  if($file=='css' or $file=='js' or $file=='icons'){
				  echo '# Updating '.$file.'<br />';
				  if (do_extract($src.$file, './'.$file)) rmdir($src.$file);  
			  } else {
				  if (copy($src.$file, $destination.$file)) {
					echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$file.' ... OK<br />';
					unlink($src.$file);
				  }
			  }
		  }
		}
		return true;
	};
	
	echo '# Downloading update... ';
	if(file_put_contents('update.zip', file_get_contents('https://bitbucket.org/ice8lue/reader/get/master.zip'))) {
		echo 'OK<br />';
		echo '# Extracting... ';
		$zip = new ZipArchive;
		$res = $zip->open('update.zip');
		if ($res === TRUE) {
		 $zip->extractTo('.');
		 $zip->close();
		 echo 'OK<br />';
		 
		 echo '# Checking version... ';
		 $files = scandir('.');
		 // Cycle through all source files
		 foreach ($files as $file) {
			 if($file != 'css' and $file != 'icons' and $file != 'js' and is_dir($file)) {
			 	$dir = $file;
				$version = str_replace("ice8lue-reader-", "", $file);
			 }
		 }
		 echo '#'.$version.' found! <br />';
		 
		 echo '# Updating current version... <br />';
		 if (do_extract($dir,'.')) {
			 echo 'OK<br />';
		 } else {
			 echo 'failed. Error 1';
			 exit;
		 };
		 
		 echo '# Cleaning up...';
		 	 rmdir($dir);
			 unlink("update.zip");
		 echo 'OK <br />';
		 
		 $file = 'index.html';
		 $current = file_get_contents($file);
		 $current = str_replace('<div id="version"></div>','<div id="version">#'.$version.'</div>', $current);
		 file_put_contents($file, $current);
		 
		} else {
		 echo 'failed. Error 2';
		 exit;
		}
	} else {
		echo 'failed. Error 3';
		exit;
	};
	
	echo '# Update done!';

?>